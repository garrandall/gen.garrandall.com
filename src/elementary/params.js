import {
  PARAM_TYPES,
  getURLParams
} from "../shared/params.js";

const paramDefines = {
  width: {
    type: PARAM_TYPES.INT,
    max: window.innerWidth,
    min: 1,
    def: window.innerWidth / 2
  },
  height: {
    type: PARAM_TYPES.INT,
    max: window.innerHeight,
    min: 1,
    def: window.innerHeight / 2
  },
  rule: {
    type: PARAM_TYPES.INT,
    def: 30,
    max: 255,
    min: 0
  },
  init: {
    type: PARAM_TYPES.ENUM,
    def: "single",
    options: ["single", "random"]
  },
  stretch: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    label: "Stretch to Fit Screen"
  },
  interval: {
    type: PARAM_TYPES.INT,
    def: 20,
    hidden: true
  },
  active: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  },
  controls: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  }
};

const params = getURLParams(paramDefines);

console.log("PARAMS LOADED:", params);

export {
  params,
  paramDefines
}
