import * as THREE from "three";

import ColorSchemes from "../shared/color-schemes.js";
import WebGLScene2D from "../shared/webgl-scene-2d.js";
import { params } from "./params.js";

// shaders
import stateShader from "./shaders/stateShader.glsl";
import vertexShader from "../shared/shaders/passThruVertexShader.glsl";
import fragmentShader from "./shaders/fragmentShader.glsl";

const STATES = 2;

const colors = ColorSchemes.getEquidistantColorSet({n: 2});

const scene = new WebGLScene2D({
  width: params.stretch ? window.innerWidth : params.width,
  height: params.stretch ? window.innerHeight : params.height,
  active: params.active
});

// initialize data
const data = new Float32Array(params.width * params.height * 4);
let spawn;

switch(params.init) {
  case "random":
    spawn = (i) => i < params.width ? Math.floor(Math.random() * STATES) : 0;
    break;
  case "single":
  default:
    spawn = (i) => i === Math.floor(params.width / 2) ? 1 : 0;
    break;
}

for (let i = 0; i < params.width * params.height; i++) {
  let r = spawn(i);

  data[i*4  ] = (r >> 3) & 1;
  data[i*4+1] = (r >> 2) & 1;
  data[i*4+2] = (r >> 1) & 1;
  data[i*4+3] = r & 1;
}

scene.addComputePass({
  width: params.width,
  height: params.height,
  textureRef: "stateTexture",
  shader: stateShader,
  data,
  uniforms: {
    rule         : new THREE.Uniform(params.rule)
  }
});

scene.initComputePasses(params.interval);

scene.initRender({
  fragmentShader,
  vertexShader,
  uniforms: {
    colors: new THREE.Uniform(colors.map(color => ColorSchemes.colorToVec(color)))
  },
  defines: {
    STATES
  }
});

export default scene;
