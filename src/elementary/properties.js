import * as CONSTANTS from "../shared/constants.js";

export const title = "Elementary CA";
export const route = "elementary";
export const description = "A one-dimensional CA where a cell's next state is determined by its own state and its two neighbors.";
export const info = "https://en.wikipedia.org/wiki/Elementary_cellular_automaton";
export const source = CONSTANTS.BASE_REPO_URL + "/tree/master/src/" + route;
export const site = CONSTANTS.BASE_SITE_URL;
