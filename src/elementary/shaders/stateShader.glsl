// resolution: resolution of texture
//
// rule: binary encoded rule
//
// stateTexture: texture2D with state of cells
//
@import ../../shared/shaders/intStateTex;

uniform int rule;

int getCellState(vec2 cellRef) {
  vec2 safeRef = wrapCellRef(cellRef, resolution.xy);

  vec4 stateTex = texture2D(stateTexture, safeRef / resolution.xy);
  return stateFromTex(stateTex);
}

int getSuccessorState(vec2 cellRef) {
  float neighborhood = dot(vec3(
    getCellState(cellRef - vec2(1, 0)) * 4,
    getCellState(cellRef) * 2,
    getCellState(cellRef + vec2(1, 0)) * 1
  ), vec3(1.0));

  return int(mod(floor(float(rule) / pow(2., neighborhood)), 2.));
}

void main()
{
  if (gl_FragCoord.y > 1.) {
    gl_FragColor = stateToTex(getCellState(gl_FragCoord.xy - vec2(0., 1.)));
  } else {
    gl_FragColor = stateToTex(getSuccessorState(gl_FragCoord.xy));
  }
}
