# Elementary Cellular Automaton

Simulates a 1-Dimensional [Elementary Cellular Automaton](https://en.wikipedia.org/wiki/Elementary_cellular_automaton) across many iterations and stitches them into a 2D image.

![](sample.png)

## URL Parameters

| Parameter       | Type | Description |
|-----------------|------|-------------|
| `width`         | Int  | Width of canvas in pixels |
| `height`        | Int  | Height of canvas in pixels |
| `rule`          | Int from 0 to 255 | Integer encoding behavior for every configuration. See [here](https://en.wikipedia.org/wiki/Elementary_cellular_automaton#The_numbering_system) for how this is interpreted. |
| `init`          | `random`\|`single` | Initial state. Either randomly alive or dead, or only one cell is alive. Default is random. |
| `stretch`       | Boolean  | Stretch canvas to fit. Comes into play if the `width` and `height` attributes are smaller than the window's dimensions.
