

// https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
function mod(n, m) {
  return ((n % m) + m) % m;
}

// https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

const RULE_REGEX_1 = /^S?([0-9]*)\/B?([0-9]*)([MV]?)([0-9]*)$/i;
const RULE_REGEX_2 = /^B?([0-9]*)\/S?([0-9]*)([MV]?)([0-9]*)$/i;

function parseRule(rule) {
  let extracted = RULE_REGEX_1.exec(rule);

  if (extracted) {
    return {
      born: extracted[2],
      survive: extracted[1]
    };
  }

  extracted = RULE_REGEX_2.exec(rule);

  return {
    born: extracted[1].split("").map(i => parseInt(i)),
    survive: extracted[2].split("").map(i => parseInt(i))
  };
}

function isObject(obj) {
  return obj === Object(obj);
}

export {
  mod,
  shuffle,
  parseRule,
  isObject
};
