import { mod, shuffle } from "./utils.js";
import { Vector3 } from "three";

const H = 360;
const S = 100;
const B = 100;

const KANDINSKY = [
  { h: 215, s: 74, b: 68 },
  { h: 157, s: 96, b: 66 },
  { h: 359, s: 72, b: 79 },
  { h: 207, s: 13, b: 27 },
  { h: 197, s: 47, b: 86 },
  { h: 49,  s: 88, b: 90 },
  { h: 352, s: 28, b: 82 },
  { h: 317, s: 65, b: 47 },
  { h: 252, s: 2,  b: 99 },
  { h: 25,  s: 77, b: 98 },
  { h: 347, s: 46, b: 33 },
  { h: 158, s: 62, b: 44 }
];

const WHITE = { h: 0, s: 0, b: 100 };
const BLACK = { h: 0, s: 0, b: 0 };

function _h() {
  return Math.floor(Math.random() * H);
}

function _s() {
  return Math.floor(Math.random() * S * 0.3 + (S * 0.7));
}

function _b() {
  return Math.floor(Math.random() * B * 0.3 + (B * 0.7));
}

class ColorSchemes {
  static get H() { return H; }
  static get S() { return S; }
  static get B() { return B; }
  static get BLACK() { return BLACK; }
  static get WHITE() { return WHITE; }

  static colorToVec({h, s, b}) {
    return new Vector3(
      h / this.H,
      s / this.S,
      b / this.B
    );
  }

  static kandinsky() {
    return shuffle([...KANDINSKY]);
  }

  static blackWhite() {
    return [ BLACK, WHITE ];
  }

  static whiteBlack() {
    return [ WHITE, BLACK ];
  }

  static monochromatic({ b, s } = {}) {
    if (b === undefined) b = _b();
    if (s === undefined) s = _s();

    const h = Math.floor(Math.random() * this.H);

    return { h, s, b };
  }

  static getEquidistantColorSet({ b, s, h, n } = {}) {
    if (h === undefined) h = _h();
    if (b === undefined) b = _b();
    if (s === undefined) s = _s();

    const colors = [];
    let newH = h;
    let intH = this.H / n;

    for (let i = 0; i < n; i++) {
      colors.push({ h: newH, s, b });
      newH = mod(newH + intH, this.H);
    }

    return colors;
  }

  static shades(n) {
    const h = 0;
    const s = 0;
    let b = _b();

    const shades = [];

    for (let i = 0; i < n; i++) {
      b = mod(b + B / n, B);
      shades.push({ h, s, b});
    }

    return shades;
  }

  static complementary({ b, s } = {}) {
    const h = Math.floor(Math.random() * H);
    const n = 2;

    return this.getEquidistantColorSet({ h, s, b, n });
  }

  static triadic({ b, s } = {}) {
    const h = Math.floor(Math.random() * H);
    const n = 3;

    return this.getEquidistantColorSet({ h, s, b, n });
  }

  static tetradic({ b, s } = {}) {
    const h = Math.floor(Math.random() * H);
    const n = 4;

    return this.getEquidistantColorSet({ h, s, b, n });
  }

  static analogous({ b, s, n=3 } = {}) {
    const h = Math.floor(Math.random() * H);

    return this.getEquidistantColorSet({ h, s, b, n });
  }

  static incrementColor(color, n) {
    const h = mod(color.h + this.H / n, this.H);
    return {h, s: color.s, b: color.b}
  }

  static incrementColorVec(color, n) {
    const x = mod(color.x + this.H / n, this.H);
    return {x, y: color.y, z: color.z}
  }
}

export default ColorSchemes;
