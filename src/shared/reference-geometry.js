import * as THREE from "three";

const swarmReferenceGeometry = (
  vertices,      // vertices for one instance of this geometry
  width,         // width of texture to reference
  height,        // height of texture to reference
  {
    vertexCom=3, // components of vertices
    scale=1.0,   // optional scaling value
    rotateX=0.0, // optional rotation values
    rotateY=0.0,
    rotateZ=0.0
  } = {}
) => {
  const SwarmReferenceGeometry = function() {
    THREE.BufferGeometry.call(this);

    const referCom = 2; // references are for 2Dtextures, have 2 components

    const instances = width * height;

    const vPer = vertices.length;
    const rPer = (vertices.length / vertexCom) * referCom;

    const positions = new THREE.BufferAttribute(new Float32Array(instances * vPer), vertexCom);
    const references = new THREE.BufferAttribute( new Float32Array(instances * rPer), referCom);

    console.log(vPer, rPer);

    for (let i = 0; i < instances; i++) {
      for (let j = 0; j < vPer; j++) {
        positions.array[i*vPer+j] = vertices[j];
      }
      for (let j = 0; j < rPer; j += 2) {
        references.array[i*rPer+j]   = (i % width) / width;
        references.array[i*rPer+j+1] = Math.ceil(i / height) / height;
      }
    }

    console.log(positions.array);
    console.log(references.array);

    this.setAttribute("position", positions);
    this.setAttribute("reference", references);

    this.scale(scale, scale, scale);
    this.rotateX(rotateX);
    this.rotateY(rotateY);
    this.rotateZ(rotateZ);
  };

  SwarmReferenceGeometry.prototype = Object.create(THREE.BufferGeometry.prototype);

  return SwarmReferenceGeometry;
}

export default swarmReferenceGeometry;

