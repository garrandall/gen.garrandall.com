import * as dat from 'dat.gui';

import {
  isObject
} from "./utils.js";

import {
  PARAM_TYPES
} from "./params.js"

let gui;

function setTitle(title) {
  document.title = title;
}

function encodeParams(params) {
  return "?" + Object.keys(params).map(key => {
    let paramVal = params[key]
    if (isObject(paramVal)) {
      paramVal = paramVal.id
    }
    return encodeURIComponent(key) + '=' + encodeURIComponent(paramVal)
  }).join('&');
}

function addParam(gui, params, paramDefine, paramName) {
  if (paramDefine.hidden) return;

  let control;

  if ('options' in paramDefine) {
    control = gui.add(params, paramName, paramDefine.options);
  } else {
    control = gui.add(params, paramName);
  }

  // TODO: placing step after setting max/min seems to not properly
  // apply step interval. I should dig deeper into this and maybe open
  // an issue in the module's repo.
  if (paramDefine.type === PARAM_TYPES.INT) control.step(1);
  if ('min' in paramDefine) control.min(paramDefine.min);
  if ('max' in paramDefine) control.max(paramDefine.max);
}

function getButtonRow() {
  let buttonRow = document.getElementById('control-buttons');

  if (buttonRow == null) {
    buttonRow = document.createElement('div');
    buttonRow.classList = "control-buttons";
    buttonRow.id = "control-buttons";
    gui.domElement.appendChild(buttonRow);
  }

  return buttonRow;
}

function addLink(text, href, color) {
  const buttonRow = getButtonRow();

  const link = document.createElement('a');
  link.textContent = text;
  link.href = href;
  link.target = "blank";
  link.classList = "button-row-button";

  if (color) link.style.borderBottomColor = color;

  buttonRow.appendChild(link);
}

function addDesc(content, title) {
  const div = document.createElement('div');
  const h = document.createElement('h1');
  const p = document.createElement('p');
  h.textContent = title;
  p.innerHTML = content;

  div.classList = "control-p";
  div.appendChild(h);
  if (content && content.length > 0) div.appendChild(p);
  gui.domElement.insertBefore(div, gui.domElement.firstChild);
}

function addButton(text, onclick, color) {
  const buttonRow = getButtonRow();

  const button = document.createElement('button');
  button.textContent = text;
  button.onclick = onclick;
  button.classList = "button-row-button";

  if (color) button.style.borderBottomColor = color;

  buttonRow.appendChild(button);
}


function createControlPanel({
  params,
  paramDefines,
  title
}) {
  setTitle(title + " - " + window.location.hostname);

  gui = new dat.GUI({name: title, closeOnTop: true});

  Object.keys(params).forEach((paramName) => {
    addParam(gui, params, paramDefines[paramName], paramName);
  });
}

export {
  createControlPanel,
  addButton,
  addLink,
  addDesc,
  encodeParams
};
