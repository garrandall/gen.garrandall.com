int stateFromTex(vec4 texState) {
  int state = 0;
  if (texState.x > 0.5) state += 8;
  if (texState.y > 0.5) state += 4;
  if (texState.z > 0.5) state += 2;
  if (texState.a > 0.5) state += 1;

  return state;
}

float stateFromTexFloat(vec4 texState) {
  return dot(texState, vec4(8.0, 4.0, 2.0, 1.0));
}

vec4 stateToTex(int state) {
  vec4 sVec = vec4(0.0);
  int s = state;
  if (s >= 8) { sVec.r = 1.0; s -= 8; }
  if (s >= 4) { sVec.g = 1.0; s -= 4; }
  if (s >= 2) { sVec.b = 1.0; s -= 2; }
  if (s >= 1) { sVec.a = 1.0; s -= 1; }
  return sVec;
}

vec2 wrapCellRef(vec2 v, vec2 res) {
  return v - (res * floor(v/res));
}
