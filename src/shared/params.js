import {
  isObject
} from "./utils.js";

const PARAM_TYPES = {
  INT: "int",
  STRING: "string",
  BOOLEAN: "boolean",
  ENUM: "enum"
}

function encodeFormData(formData) {
  let a = [];
  for (let [key, val] of formData.entries()) {
    a.push(key + "=" + encodeURIComponent(val));
  }
  return "?" + a.join("&");
}

function getURLParamString(name, def=null) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || def;
}

function getURLParam(name, {type, def}) {
  const raw = getURLParamString(name, def);

  switch(type) {
    case PARAM_TYPES.INT:
      return parseInt(raw, 10);
    case PARAM_TYPES.BOOLEAN:
      return raw == true || raw == "true";
    case PARAM_TYPES.STRING:
    default:
      return raw;
  }
}

function getURLParams(paramDefines) {
  const params = {};

  Object.keys(paramDefines).forEach((name) => {
    params[name] = getURLParam(name, paramDefines[name]);
  });

  return params;
}

export {
  PARAM_TYPES,
  getURLParams,
  encodeFormData
};
