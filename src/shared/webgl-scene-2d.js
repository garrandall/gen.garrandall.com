import * as THREE from "three";
import * as PP from "postprocessing";
import GPUComputationRenderer from './gpu-computation-renderer.js';

class ComputePass {
  constructor({
    width,
    height,
    data,
    shader,
    uniforms={},
    defines={},
    textureRef="computeTexture",
    wrapping=THREE.ClampToEdgeWrapping,
    renderer
  } = {}) {
    if (width * height * 4 !== data.length) {
      throw Error(`Given initial compute data must contain ${width*height*4} elements - contains ${data.length}`);
    }

    // set up
    this.renderer = renderer;
    this.texture = this.renderer.createTexture();
    this.variable = this.renderer.addVariable(textureRef, shader, this.texture);

    // load data
    const computeData = this.texture.image.data;
    for (let i = 0; i < data.length; i++) computeData[i] = data[i];

    // add user-specified defines
    this.variable.material.defines = {
      ...this.variable.material.defines,
      ...defines
    };

    // add user-specified uniforms
    this.variable.material.uniforms = {
      ...this.variable.material.uniforms,
      ...uniforms
    };

    this.uniforms = this.variable.material.uniforms;

    // set wrapping behavior
    this.variable.wrapS = wrapping;
    this.variable.wrapT = wrapping;

    this.ref = textureRef;
  }

  init() {
    return this.renderer.init();
  }

  compute() {
    return this.renderer.compute();
  }

  setVariableDependencies(variables) {
    this.renderer.setVariableDependencies(this.variable, variables);
  }

  getTarget() {
    return this.renderer.getCurrentRenderTarget(this.variable).texture;
  }
}

/**
 * typical usage
 *
 * const scene = new Scene2D(...);
 * scene.attachToDOM();
 * scene.addComputePass(...);
 * scene.initComputePasses(...);
 * scene.initRender(...);
 * scene.run();
 *
 * if you decide to capture an image,
 *
 * scene.capture();
 *
 * other methods:
 *
 * scene.setUniformUpdateHandler(handler);
 * scene.pause();
 * scene.play();
 * scene.toggle();
 */
class WebGLScene2D {
  constructor({
    width=window.innerWidth,
    height=window.innerHeight,
    active=true
  } = {}) {
    this.width  = width;
    this.height = height;

    this.resolution = new THREE.Vector2(width, height);
    this.scene      = new THREE.Scene();
    this.camera     = new THREE.OrthographicCamera();
    this.renderer   = new THREE.WebGLRenderer();

    this.computePasses = [];

    this.computeUniforms = {};
    this.renderUniforms  = {};

    this.active = active;
    this.shouldCapture = false;

    this.initEffectComposer();
  }

  attachToDOM() {
    this.renderer.setSize(this.width, this.height);
    this.composer.setSize(this.width, this.height);
    document.body.appendChild(this.renderer.domElement);
  }

  initEffectComposer(...effects) {
    this.composer = new PP.EffectComposer(this.renderer);
    this.composer.addPass(new PP.RenderPass(this.scene, this.camera));

    const effectPass = new PP.EffectPass(this.camera, ...effects);
    effectPass.renderToScreen = true;
    this.composer.addPass(effectPass);
  }

  addComputePass({
    width,
    height,
    data,
    shader,
    uniforms={},
    defines={},
    textureRef="computeTexture",
    wrapping=THREE.ClampToEdgeWrapping
  } = {}) {
    if (this.computePasses.map(c => c.textureRef === textureRef).includes(true)) {
      throw Error(`${textureRef}: this compute texture ref is already in use`);
    }

    if (!this.computeRenderer) {
      this.computeRenderer = new GPUComputationRenderer(width, height, this.renderer);
    }

    const enrichedUniforms = {
      ...uniforms,
      resolution: new THREE.Uniform(new THREE.Vector2(width, height))
    };

    const computePass = new ComputePass({
      width,
      height,
      data,
      shader,
      uniforms: enrichedUniforms,
      defines,
      textureRef,
      wrapping,
      renderer: this.computeRenderer
    });

    this.computePasses.push(computePass);
  }

  initRender({
    fragmentShader,
    vertexShader,
    uniforms={},
    defines={}
  } = {}) {
    const computeUniforms = {};

    // if we're using the compute renderer, we have to ensure some
    // specific uniforms are set.
    this.computePasses.forEach(computePass => {
      computeUniforms[computePass.ref] = new THREE.Uniform(computePass.getTarget());
    });

    this.renderUniforms = {
      resolution: new THREE.Uniform(this.resolution),
      ...computeUniforms,
      ...uniforms
    }

    const material = new THREE.RawShaderMaterial({
      fragmentShader,
      vertexShader,
      uniforms: this.renderUniforms,
      defines
    });

    // set up a quad to span screen and render computed state onto
    const vertices = new Float32Array([
      -1.0, -1.0,  1.0, -1.0, -1.0,  1.0,
       1.0,  1.0, -1.0,  1.0,  1.0, -1.0
    ]);

    const geometry = new THREE.BufferGeometry();
    geometry.setAttribute("position", new THREE.BufferAttribute(vertices, 2));

    const triangle = new THREE.Mesh(geometry, material);
    triangle.frustumCulled = false;
    this.scene.add(triangle);
  }

  // this method is called after compute passes have all been initialized
  // and assumes that each compute pass depends on the prior state of all
  // compute passes.
  initComputePasses(interval=0) {
    const vars = this.computePasses.map(c => c.variable);

    this.computePasses.forEach(c => c.setVariableDependencies(vars));

    const error = this.computeRenderer.init();
    if (error != null) console.error(error);

    this.computeInterval = interval;
    this.computeUniforms = {}
    this.computePasses.forEach(compPass => this.computeUniforms[compPass.ref] = compPass.uniforms)
  }

  setUniformUpdateHandler(uniformUpdateHandler) {
    this.uniformUpdateHandler = uniformUpdateHandler;
  }

  render() {
    let time = Date.now();

    if (
      (!this.computeInterval ||
        time - this.timeSinceCompute > this.computeInterval) &&
      this.active
    ) {
      this.computeRenderer.compute();
      this.timeSinceCompute = time;
    }

    if (this.uniformUpdateHandler) {
      this.uniformUpdateHandler(
        this.computeUniforms,
        this.renderUniforms
      );
    }

    this.computePasses.forEach(computePass => {
      this.renderUniforms[computePass.ref].value = computePass.getTarget();
    });

    this.renderer.setRenderTarget(null);
    this.composer.render(time - this.timeSinceRender);

    this.timeSinceRender = time;
  }

  run() {
    this.timeSinceCompute = Date.now();
    this.timeSinceRender = this.timeSinceCompute;
    this._animate();
  }

  capture() {
    this.shouldCapture = true;
  }

  pause() {
    this.active = false;
  }

  play() {
    this.active = true;
  }

  toggle() {
    this.active = !this.active;
  }

  _animate() {
    this.render();
    if (this.shouldCapture) this._handleCapture();
    requestAnimationFrame(this._animate.bind(this));
  }

  _handleCapture() {
    this.shouldCapture = false;
    const url = this.renderer.domElement.toDataURL("image/png", 1);
    window.open(url, '_blank');
  }
}

export default WebGLScene2D;
