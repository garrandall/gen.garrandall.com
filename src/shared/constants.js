const BASE_SITE_URL = "/";
const BASE_REPO_URL = "https://gitlab.com/garrandall/gen.garrandall.com";

export {
  BASE_SITE_URL,
  BASE_REPO_URL
};
