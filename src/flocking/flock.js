import { Vector3, Vector2 } from "three";

const BOUNDS = {
  CUBE: "cube"
};

const INIT = {
  RANDOM: "random",
  ORIGIN: "origin"
};

class Boid {
  constructor(loc, vel) {
    this._loc = loc;
    this._vel = vel;
  }

  get loc() { return this._loc; }
  get vel() { return this._vel; }
}

class Flock {
  static get BOUNDS() {
    return BOUNDS;
  }

  static get INIT() {
    return INIT;
  }

  constructor({
    flockSize=10,
    flockInit=INIT.RANDOM,
    bounds=BOUNDS.CUBE,
    boundsRadius=1,
    dimensions=2
  } = {}) {
    this._dimensions = dimensions;

    // bounds
    this._bounds = bounds;
    this._boundsRadius = boundsRadius;
    this._boundsOrigin = dimensions === 2 ? new Vector2(0) : new Vector3(0);

    // flock
    this._flockSize = flockSize;
    this._flock = this.initFlock(flockInit);
  }

  get flock() {
    return this._flock;
  }

  initFlock(initMethod) {
    const flock = [];
    switch(initMethod) {
      case INIT.ORIGIN:
        for (let i = 0; i < this._flockSize; i++) {
          let vec = this.dimensions === 2 ? new Vector2() : new Vector3();
          flock.push(new Boid(
            vec.copy(this._boundsOrigin),
            this.dimensions === 2 ? new Vector2(0.01) : new Vector3(0.01) // TODO: fix
          ));
        }
        break;
      case INIT.RANDOM:
          for (let i = 0; i < this._flockSize; i++) {
            flock.push(this.getRandomInBounds());
          }
        break;
      default:
        throw `initMethod ${initMethod} not supported`;
    }
    return flock;
  }

  getRandomInBounds() {
    let loc;
    let vel;
    switch(this._bounds) {
      case BOUNDS.CUBE:
        if (this._dimensions === 2) {
          loc = new Vector2(
            Math.random() * this._boundsRadius * 2 - this._boundsRadius,
            Math.random() * this._boundsRadius * 2 - this._boundsRadius
          );

          vel = new Vector2(
            Math.random() - 0.5,
            Math.random() - 0.5
          );

          return new Boid(loc, vel);
        } else {
          loc = new Vector3(
            Math.random() * this._boundsRadius * 2 - this._boundsRadius,
            Math.random() * this._boundsRadius * 2 - this._boundsRadius,
            Math.random() * this._boundsRadius * 2 - this._boundsRadius
          );

          vel = new Vector3(
            Math.random() - 0.5,
            Math.random() - 0.5,
            Math.random() - 0.5
          );

          return new Boid(loc, vel);
        }
      default:
        throw `bounds type ${this._bounds} not supported`;
    }
  }
}


export default Flock;
