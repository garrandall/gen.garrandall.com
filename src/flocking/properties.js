import * as CONSTANTS from "../shared/constants.js";

export const title = "Flocking";
export const route = "flocking";
export const description = "";
export const info = "https://en.wikipedia.org/wiki/Boids";
export const source = CONSTANTS.BASE_REPO_URL + "/tree/master/src/" + route;
export const site = CONSTANTS.BASE_SITE_URL;
