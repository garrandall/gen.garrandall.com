import {
  PARAM_TYPES,
  getURLParams
} from "../shared/params.js";

const paramDefines = {
  flockSize: {
    type: PARAM_TYPES.INT,
    def: 512,
    max: 2048,
    min: 1
  },
  controls: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  }
};

const params = getURLParams(paramDefines);

console.log("PARAMS LOADED:", params);

export {
  params,
  paramDefines
}
