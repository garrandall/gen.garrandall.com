import index from "./index.html";
import sharedStyles from "../shared/styles.css";

import { params, paramDefines } from "./params";
import * as PROPERTIES from "./properties.js";
import "./init-scene.js";
import {
  createControlPanel,
  encodeParams,
  addDesc,
  addButton,
  addLink
} from "../shared/controls.js";

if (params.controls) {
  createControlPanel({ ...PROPERTIES, params, paramDefines });

  addDesc(null, PROPERTIES.title);

  addButton("apply", () => {
    const route = window.location.href.split("?")[0];
    window.location = route + encodeParams(params);
  }, "#1ed36f");
  addLink("src", PROPERTIES.source);
  addLink("more", PROPERTIES.site);
}
