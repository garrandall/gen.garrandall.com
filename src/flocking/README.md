# Flocking

Simulates flocking behavior developed by Craig Reynolds in 1987 in his program Boids.

Each bird in the flock has a limited range of vision around it, and balances three different behaviors to determine its heading:

- ***Seperation.*** Boids should get out of each other's way.
- ***Cohesion.*** Boids should travel towards each other to maintain a flock.
- ***Alignment.*** Boids should generally move in the same direction as their flockmates.

**TODO.** This simulation isn't quite tuned in right - the boids quite quickly condense into very tightly packed, overlapping stacks. This behavior eventually converges to a single very condensed flock.

## URL Parameters

| Parameter       | Type | Description |
|-----------------|------|-------------|
| `flockSize`     | Int  | Number of boids |
