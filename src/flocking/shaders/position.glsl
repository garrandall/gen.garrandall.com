uniform float time;
uniform float delta;

vec3 wrap(vec3 v) {
  return mod(v + BOUNDS, 2. * BOUNDS) - BOUNDS;
}

void main()
{
  vec3 texVelocity = texture2D(textureVelocity, gl_FragCoord.xy / resolution.xy).xyz;
  vec3 texPosition = texture2D(texturePosition, gl_FragCoord.xy / resolution.xy).xyz;

  texPosition = wrap(texPosition);

  gl_FragColor = vec4(texPosition + texVelocity * delta * 5., 1.0);
}
