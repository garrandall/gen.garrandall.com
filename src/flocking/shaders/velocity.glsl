uniform float time;
uniform float delta;

uniform float vision;

uniform float alignmentWeight;
uniform float cohesionWeight;
uniform float seperationWeight;

const float LIMIT = 0.02;

vec3 wrap(vec3 v) {
  return mod(v + BOUNDS, 2. * BOUNDS) - BOUNDS;
}

void main()
{
  vec3 texVelocity = texture2D(textureVelocity, gl_FragCoord.xy / resolution.xy).xyz;
  vec3 texPosition = texture2D(texturePosition, gl_FragCoord.xy / resolution.xy).xyz;

  vec3 alignment  = vec3(0.);
  vec3 cohesion   = vec3(0.);
  vec3 seperation = vec3(0.);
  int  neighbors  = 0;

  vec2 otherRef;
  vec3 otherPosition;
  vec3 otherVelocity;
  float otherDist;

  float totalWeight = alignmentWeight + cohesionWeight + seperationWeight;

  for (float x = 0.; x < resolution.x; x++) {
    for (float y = 0.; y < resolution.y; y++) {
      otherRef      = vec2(x, y) / resolution.xy;
      otherPosition = texture2D(texturePosition, otherRef).xyz;
      otherDist     = distance(texPosition, otherPosition);

      if (otherDist > 0.0001 && otherDist < vision) {
        alignment  += texture2D(textureVelocity, otherRef).xyz;
        cohesion   += otherPosition;
        seperation -= otherPosition - texPosition;
        neighbors  += 1;
      }
    }
  }

  if (neighbors > 0) {
    float n = float(neighbors);
    alignment  = ((alignment / n) - texVelocity) * LIMIT; // arbitrary
    cohesion   = ((cohesion / n) - texPosition) * LIMIT;
    seperation = ((seperation / n) - texPosition) * LIMIT;

    texVelocity += alignment + cohesion + seperation;
  }

  gl_FragColor = vec4(normalize(texVelocity), 1.0);
}
