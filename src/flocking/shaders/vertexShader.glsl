attribute vec2 reference;

uniform sampler2D texturePosition;
uniform sampler2D textureVelocity;

uniform float time;
uniform float delta;

void main() {
  vec3 pos = texture2D(texturePosition, reference.xy).xyz;
  vec3 vel = normalize(texture2D(textureVelocity, reference.xy).xyz);

  vec3 newPosition = mat3(modelMatrix) * position;

  vel.z *= -1.;
  float xz = length( vel.xz );
  float xyz = 1.;
  float x = sqrt( 1. - vel.y * vel.y );

  float cosry = vel.x / xz;
  float sinry = vel.z / xz;
  float cosrz = x / xyz;
  float sinrz = vel.y / xyz;

  mat3 maty =  mat3(
    cosry, 0, -sinry,
    0    , 1, 0     ,
    sinry, 0, cosry
  );

  mat3 matz =  mat3(
    cosrz , sinrz, 0,
    -sinrz, cosrz, 0,
    0     , 0    , 1
  );

  newPosition = maty * matz * newPosition;
  newPosition += pos;

  gl_Position = projectionMatrix * viewMatrix * vec4(newPosition, 1.0);
}
