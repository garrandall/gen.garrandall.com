import * as THREE from "three";

import GPUComputationRenderer from '../shared/gpu-computation-renderer.js';

import ColorSchemes from "../shared/color-schemes.js";
import swarmReferenceGeometry from "../shared/reference-geometry.js";
import Flock from "./flock.js";
import { params } from "./params.js";

// shaders
import velocityShader from "./shaders/velocity.glsl";
import positionShader from "./shaders/position.glsl";
import vertexShader from "./shaders/vertexShader.glsl";
import fragmentShader from "./shaders/fragmentShader.glsl";

/**
 * unrollGeometry
 * @param {THREE.Geometry} geometry
 */
function unrollGeometry({ faces, vertices }) {
  const unrolled = [];
  faces.forEach(({a, b, c}) => unrolled.push(
    vertices[a].x, vertices[a].y, vertices[a].z,
    vertices[b].x, vertices[b].y, vertices[b].z,
    vertices[c].x, vertices[c].y, vertices[c].z
  ));
  return unrolled;
}

const [
  COLOR,
  BG
] = ColorSchemes.kandinsky();

const BOUNDS_RADIUS = 10.0;

const WIDTH = params.flockSize;
const HEIGHT = 1;
const FLOCK_SIZE = WIDTH * HEIGHT;

const SCENE = new THREE.Scene();
const CAMERA = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 1000);
const RENDERER = new THREE.WebGLRenderer();
const COMPUTE = new GPUComputationRenderer(WIDTH, HEIGHT, RENDERER);

let positionVariable;
let velocityVariable;
let positionUniforms;
let velocityUniforms;
let boidUniforms;
let lastRender = Date.now();

const FLOCK = new Flock({
  flockSize: FLOCK_SIZE,
  flockInit: Flock.INIT.RANDOM,
  boundsRadius: BOUNDS_RADIUS,
  dimensions: 3
});

const coneGeometry = new THREE.ConeGeometry(0.25);
const packedVertices = unrollGeometry(coneGeometry);
const ReferenceGeometry = swarmReferenceGeometry(
  packedVertices, WIDTH, HEIGHT, { scale: 0.6, rotateX: Math.PI / 2 }
);

// a Three.js Flocking Simulation
const init = () => {
  SCENE.background = new THREE.Color(`hsl(${BG.h}, ${BG.s}%, ${BG.b}%)`);

  CAMERA.position.z = 10;
  RENDERER.setPixelRatio( window.devicePixelRatio );
  RENDERER.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(RENDERER.domElement);

  initComputeRenderer();
  initBoids();

  return SCENE;
};

const initComputeRenderer = () => {
  const positionTexture = COMPUTE.createTexture();
  const velocityTexture = COMPUTE.createTexture();

  const positionsData = positionTexture.image.data;
  const velocitiesData = velocityTexture.image.data;

  for (let i = 0; i < FLOCK.flock.length; i++) {
    let boid = FLOCK.flock[i];

    positionsData[i*4]   = boid.loc.x;
    positionsData[i*4+1] = boid.loc.y;
    positionsData[i*4+2] = boid.loc.z;
    positionsData[i*4+3] = 1;

    velocitiesData[i*4]   = boid.vel.x;
    velocitiesData[i*4+1] = boid.vel.y;
    velocitiesData[i*4+2] = boid.vel.z;
    velocitiesData[i*4+3] = 1;
  }

  velocityVariable = COMPUTE.addVariable("textureVelocity", velocityShader, velocityTexture);
  positionVariable = COMPUTE.addVariable("texturePosition", positionShader, positionTexture);
  COMPUTE.setVariableDependencies(velocityVariable, [positionVariable, velocityVariable]);
  COMPUTE.setVariableDependencies(positionVariable, [positionVariable, velocityVariable]);

  positionUniforms = positionVariable.material.uniforms;
  velocityUniforms = velocityVariable.material.uniforms;

  positionUniforms.time = { value: 0.0 };
  positionUniforms.delta = { value: 0.0 };
  velocityUniforms.time = { value: 1.0 };
  velocityUniforms.delta = { value: 0.0 };
  velocityUniforms.separationWeight = { value: 1.0 };
  velocityUniforms.alignmentWeight = { value: 1.0 };
  velocityUniforms.cohesionWeight = { value: 1.0 };
  velocityUniforms.vision = { value: 1.0 };

  positionVariable.material.defines.BOUNDS = BOUNDS_RADIUS.toFixed(2);
  velocityVariable.material.defines.BOUNDS = BOUNDS_RADIUS.toFixed(2);

  positionVariable.wrapS = THREE.ClampToEdgeWrapping;
  positionVariable.wrapT = THREE.ClampToEdgeWrapping;
  velocityVariable.wrapS = THREE.ClampToEdgeWrapping;
  velocityVariable.wrapT = THREE.ClampToEdgeWrapping;

  let error = COMPUTE.init();

  if (error !== null) {
    console.error(error);
  }
};

const initBoids = () => {
  const geometry = new ReferenceGeometry();

  // THREE.ShaderMaterial
  boidUniforms = {
    color: { value: new THREE.Color(`hsl(${COLOR.h}, ${COLOR.s}%, ${COLOR.b}%)`) },
    texturePosition: { value: null },
    textureVelocity: { value: null },
    time: { value: 1.0 },
    delta: { value: 0.0 }
  };

  const material = new THREE.ShaderMaterial({
    uniforms: boidUniforms,
    vertexShader,
    fragmentShader,
    side: THREE.DoubleSide
  });

  const mesh = new THREE.Mesh(geometry, material);
  mesh.rotation.y = Math.PI / 2;
  mesh.matrixAutoUpdate = false;
  mesh.updateMatrix();

  SCENE.add(mesh);
};

const render = () => {
  let now = Date.now();
  let delta = (now - lastRender) / 1000;
  if (delta > 1) delta = 1;
  lastRender = now;

  positionUniforms.time.value = now;
  positionUniforms.delta.value = delta;

  velocityUniforms.time.value = now;
  velocityUniforms.delta.value = delta;

  boidUniforms.time.value = now;
  boidUniforms.delta.value = delta;

  COMPUTE.compute();

  boidUniforms.texturePosition.value = COMPUTE.getCurrentRenderTarget(positionVariable).texture;
  boidUniforms.textureVelocity.value = COMPUTE.getCurrentRenderTarget(velocityVariable).texture;

  RENDERER.setRenderTarget(null);
  const error = RENDERER.render( SCENE, CAMERA );

  if (error) {
    console.error(error);
  }
};

const run = () => {
  function animate() {
    render();
    requestAnimationFrame(animate);
  }

  animate();
};

init();
run();
