import * as CONSTANTS from "../shared/constants.js";

export const title = "Flow Fields";
export const route = "flow-field";
export const description = "Uses noise to simulate the motion of particles on a flow field.";
export const info = "https://en.wikipedia.org/wiki/Vector_field";
export const source = CONSTANTS.BASE_REPO_URL + "/tree/master/src/" + route;
export const site = CONSTANTS.BASE_SITE_URL;
