import index from "./index.html";
import sharedStyles from "../shared/styles.css";
import sample from "./sample.png";

import * as p5 from "p5/lib/p5.min";

import init from "./init-scene.js";
import { params, paramDefines } from "./params.js";
import * as PROPERTIES from "./properties.js";
import {
  createControlPanel,
  encodeParams,
  addDesc,
  addButton,
  addLink
} from "../shared/controls.js";

if (params.controls) {
  createControlPanel({ ...PROPERTIES, params, paramDefines });

  addDesc(null, PROPERTIES.title);

  addButton("apply", () => {
    const route = window.location.href.split("?")[0];
    window.location = route + encodeParams(params);
  }, "#1ed36f");
  addButton("download", () => {
    let w = window.open(document.getElementById('defaultCanvas0').toDataURL(), "_blank");
    w.focus();
  }, "#2FA1D6");
  addLink("src", PROPERTIES.source);
  addLink("more", PROPERTIES.site);
}


const P5 = new p5(init);
