import { Vector, createVector } from "p5/lib/p5.min";

import { params } from "./params.js";
import ColorSchemes from "../shared/color-schemes.js";
import { mod } from "../shared/utils.js";

const KANDINSKY = ColorSchemes.kandinsky().slice(0, 3);

const colors = {
  white: () => ({ h: 0, s: 0, b: 80 }),
  green: () => ({ h: 120, s: 60, b: 80 }),
  red: () => ({ h: 320, s: 80, b: 40 }),
  rainbow: (i, t) => ({ h: t % 360, s: 60, b: 80 }),
  kandinsky: (i, t) => (KANDINSKY[i % KANDINSKY.length]),
}

const background = {
  white: 255,
  black: 0
};

/**
 * TODO: yo clean this class up
 */
class Particle {
  constructor(x, y, xMax, yMax) {
    this.pos = { x, y };
    this.prev = { x, y };
    this.vel = { x: 0, y: 0 };
    this.acc = { x: 0, y: 0 };
    this.xMax = xMax;
    this.yMax = yMax;
  }

  apply(force) {
    this.acc = { x: force.x, y: force.y};
    this.vel = {
      x: this.vel.x + this.acc.x,
      y: this.vel.y + this.acc.y
    };
    let len = Math.sqrt(this.vel.x ** 2 + this.vel.y ** 2) / (params.velocity * 0.1);
    this.vel = { x: this.vel.x / len, y: this.vel.y / len };

    this.prev = this.pos;

    this.pos = {
      x: this.pos.x + this.vel.x,
      y: this.pos.y + this.vel.y
    };

    if (this.pos.x < 0) this.prev.x = this.xMax;
    if (this.pos.x > this.xMax) this.prev.x = 0;
    if (this.pos.y < 0) this.prev.y = this.yMax;
    if (this.pos.y > this.yMax) this.prev.y = 0;

    this.pos = {
      x: mod(this.pos.x, this.xMax),
      y: mod(this.pos.y, this.yMax)
    };
  }

  draw(sk, { h, s, b }) {
    sk.stroke(h, s, b, params.alphaPercentage / 100);
    sk.line(
      this.prev.x, this.prev.y,
      this.pos.x, this.pos.y
    );
  }
}

const init = (sk) => {
  const inc = 0.01;
  let t = 0;
  let active = true;

  const particles = [];

  sk.setup = () => {
    sk.colorMode(sk.HSB, 360, 100, 100);
    sk.createCanvas(params.width, params.height);
    sk.noStroke();

    sk.background(background[params.background]);
    sk.fill(255);

    for (let i = 0; i < params.particles; i++) {
      particles.push(new Particle(
        Math.floor(Math.random() * params.width),
        Math.floor(Math.random() * params.height),
        params.width,
        params.height
      ));
    }
  }

  sk.draw = () => {
    sk.strokeWeight(params.strokeWeight);

    particles.map((p, i) => {
      let n = sk.noise(
        p.pos.x * inc,
        p.pos.y * inc,
        t * inc * params.timeFactor * 0.1
      );
      let v = Vector.fromAngle(n * 4 * Math.PI);
      p.apply(v);
      p.draw(sk, colors[params.colors](i, t));
    });

    t += 1;
  },

  sk.keyTyped = () => {
    if (sk.key === 'p') {
      active ? sk.noLoop() : sk.loop();
      active = !active;
    }
  }
}

export default init;
