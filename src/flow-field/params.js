import {
  PARAM_TYPES,
  getURLParams
} from "../shared/params.js";

const paramDefines = {
  width: {
    type: PARAM_TYPES.INT,
    max: window.innerWidth,
    min: 1,
    def: window.innerWidth,
    hidden: true
  },
  height: {
    type: PARAM_TYPES.INT,
    max: window.innerHeight,
    min: 1,
    def: window.innerHeight,
    hidden: true
  },
  particles: {
    type: PARAM_TYPES.INT,
    max: 2500,
    min: 1,
    def: 500
  },
  strokeWeight: {
    type: PARAM_TYPES.INT,
    max: 10,
    min: 1,
    def: 1
  },
  alphaPercentage: {
    type: PARAM_TYPES.INT,
    max: 100,
    min: 1,
    def: 10
  },
  velocity: {
    type: PARAM_TYPES.INT,
    max: 100,
    min: 1,
    def: 10
  },
  timeFactor: {
    type: PARAM_TYPES.INT,
    max: 100,
    min: 0,
    def: 20
  },
  colors: {
    type: PARAM_TYPES.ENUM,
    def: "rainbow",
    options: ["rainbow", "white", "green", "red", "kandinsky"]
  },
  background: {
    type: PARAM_TYPES.ENUM,
    def: "white",
    options: ["black", "white"]
  },
  controls: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  }
};

const params = getURLParams(paramDefines);

console.log("PARAMS LOADED:", params);

export {
  params,
  paramDefines
}
