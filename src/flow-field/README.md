# Flow Field Particle Simulation

Simulates the motion of many particles on a flow field whose vectors are built from Perlin noise.

![](sample.png)

## URL Parameters

| Parameter       | Type | Description |
|-----------------|------|-------------|
| `width`         | Int  | Width of canvas in pixels |
| `height`        | Int  | Height of canvas in pixels |
| `particle`      | Int  | Number of particles |
| `strokeWeight`  | Int  | Stroke Weight of lines drawn by particles |
| `alphaPercentage` | Int | from 0-100, alpha value of lines drawn by particles |
| `velocity`      | Int  | Determines velocity of particles |
| `timeFactor`    | Int  | How fast "time" passes, time being the 3rd parameter in the noise function. A higher value decreases coherence from one noise evaluation to the next. |
| `colors`        | Options | A few color options |
| `background`    | `black`\|`white` | The color of the background |
