import index from "./index.html";
import styles from "./styles.css";

import App from './home/App.svelte';

const app = new App({
	target: document.body
});
