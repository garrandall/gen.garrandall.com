import index from "./index.html";
import sharedStyles from "../shared/styles.css";
import sample from "./sample.png";

import scene from "./init-scene.js";
import { params, paramDefines } from "./params.js";
import * as PROPERTIES from "./properties.js";
import {
  createControlPanel,
  encodeParams,
  addDesc,
  addButton,
  addLink
} from "../shared/controls.js";

scene.attachToDOM();
scene.run();

if (params.controls) {
  createControlPanel({ ...PROPERTIES, params, paramDefines });

  addDesc(null, PROPERTIES.title);

  addButton("apply", () => {
    const route = window.location.href.split("?")[0];
    window.location = route + encodeParams(params);
  }, "#1ed36f");
  addButton("download", () => scene.capture(), "#2FA1D6");
  addLink("src", PROPERTIES.source);
  addLink("more", PROPERTIES.site);
}

document.onkeypress = ({ code }) => {
  switch(code) {
    case "KeyP":
      scene.toggle();
      break;
  }
}
