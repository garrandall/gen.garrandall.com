import * as THREE from "three";

import ColorSchemes from "../shared/color-schemes.js";
import WebGLScene2D from "../shared/webgl-scene-2d.js";
import { params, Init, Rules, ColorBehavior } from "./params";
import { mod } from "../shared/utils.js";

// shaders
import stateShader from "./shaders/stateShader.glsl";
import vertexShader from "../shared/shaders/passThruVertexShader.glsl";
import fragmentShader from "./shaders/fragmentShader.glsl";

let initialColor;
let persistanceFactor = 0.0;

switch(params.colorBehavior) {
  case ColorBehavior.PERSIST:
    initialColor = ColorSchemes.kandinsky()[0]
    initialColor.h = 0
    persistanceFactor = 1.0
    break;
  case ColorBehavior.FADE_OUT:
    initialColor = ColorSchemes.kandinsky()[0]
    persistanceFactor = 0.9
    break
  case ColorBehavior.LIVE_ONLY:
  default:
    initialColor = ColorSchemes.kandinsky()[0]
    break;
}

const scene = new WebGLScene2D({
  width: params.stretch ? window.innerWidth : params.width,
  height: params.stretch ? window.innerHeight : params.height,
  active: params.active
});

const ruleConfig = Rules[params.rule];

const [RANGE, BIRTH_LOWER, BIRTH_UPPER, SURVIVE_LOWER, SURVIVE_UPPER] = ruleConfig.rule;

// randomly initialize data
const data = new Float32Array(params.width * params.height * 4);
if (ruleConfig.init === Init.RANDOM) {
  for (let i = 0; i < params.width * params.height; i++) {
    let r = Math.random() > 0.6 ? 1 : 0;
    data[i*4  ] = 0;
    data[i*4+1] = 0;
    data[i*4+2] = 0;
    data[i*4+3] = r;
  }
} else if (ruleConfig.init === Init.CIRCLE_99) {
  for (let i = 0; i < params.width * params.height; i++) {
    let row = Math.floor(i / params.width) - params.height / 2;
    let col = (i % params.width) - params.width / 2;

    if (
      Math.hypot(row, col) <= 98
    ) {
      data[i*4  ] = 0;
      data[i*4+1] = 0;
      data[i*4+2] = 0;
      data[i*4+3] = 1;
    }
  }
}


scene.addComputePass({
  width: params.width,
  height: params.height,
  textureRef: "stateTexture",
  shader: stateShader,
  data,
  uniforms: {
    liveColor: new THREE.Uniform(ColorSchemes.colorToVec(initialColor)),
    persistanceFactor: new THREE.Uniform(1.0-persistanceFactor)
  },
  defines: {
    RANGE,
    BIRTH_LOWER,
    BIRTH_UPPER,
    SURVIVE_LOWER,
    SURVIVE_UPPER
  }
});

if (params.colorBehavior === ColorBehavior.PERSIST) {
  scene.setUniformUpdateHandler((computeUniforms) => {
    if (computeUniforms.stateTexture && computeUniforms.stateTexture.liveColor) {
      const old = computeUniforms.stateTexture.liveColor.value
      const x = mod(old.x + 0.005, ColorSchemes.H)
      computeUniforms.stateTexture.liveColor.value = {
        ...old,
        x
      }
    }
  });
}



scene.initComputePasses(params.interval);

scene.initRender({
  fragmentShader,
  vertexShader,
  defines: {
    NUM_STATES: 2
  }
});

export default scene;
