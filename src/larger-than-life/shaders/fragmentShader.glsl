precision highp float;

@import ../../shared/shaders/intStateTex;
@import ../../shared/shaders/hsv2rgb;

uniform sampler2D stateTexture;
uniform vec2 resolution;

void main() {
  vec4 stateTex = texture2D(stateTexture, gl_FragCoord.xy / resolution.xy);

  gl_FragColor = vec4(hsv2rgb(stateTex.xyz), 1.0);
}
