// N: num states
// X: dimensions of texture in X axis
// Y: dimensions of texture in Y axis
//
// born: num of neighbors to change a cell from dead to alive
// survive: num of neighbors to keep a cell alive
// range: radius of neighborhood
// neighborhood: box (0) or diamond (1)
//
// wrapEdges: should edges be wrapped
// edgeBehavior: if edges not wrapped, how should edges be treated?
//
// stateTexture: texture2D with state of cells
//
@import ../../shared/shaders/intStateTex;

uniform vec3 liveColor;
uniform float persistanceFactor;

vec4 getCellState(vec2 cellRef) {
  vec2 safeRef = wrapCellRef(cellRef, resolution.xy);

  vec4 stateTex = texture2D(stateTexture, safeRef / resolution.xy);
  return stateTex;
}

void main()
{
  vec4 state = getCellState(gl_FragCoord.xy);

  int neighbors = 0;

  for (int dy = -RANGE; dy <= RANGE; dy++) {
    for (int dx = -RANGE; dx <= RANGE; dx++) {
      if (getCellState(gl_FragCoord.xy + vec2(dx, dy)).a > 0.0) {
        neighbors += 1;
      }
    }
  }

  if (state.a > 0.0 && SURVIVE_LOWER <= neighbors && SURVIVE_UPPER >= neighbors) {
    gl_FragColor = vec4(liveColor, 1.0);
  } else if (state.a == 0.0 && BIRTH_LOWER <= neighbors && BIRTH_UPPER >= neighbors) {
    gl_FragColor = vec4(liveColor, 1.0);
  } else {
    gl_FragColor = vec4(mix(state.xyz, vec3(0.0), persistanceFactor), 0.0);
  }

  return;
}
