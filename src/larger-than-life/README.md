# Larger Than Life

Simulates Larger Than Life] cellular automata, which are a generalized for of Conway's Game of Life, on a live WebGL canvas.

![](sample.png)

## URL Parameters

| Parameter       | Type | Description |
|-----------------|------|-------------|
| `width`                | Int      | Width of canvas  |
| `height`               | Int      | Height of canvas |
| `rule`          | String | Pre-configured rule sets gathered from the web |
| `range`                | Int      | Radius of neighborhood. Recommended to be 1 or 2 |
| `active`        | Boolean | Is simulation running on page load, or paused? Default is true. |
| `interval`      | Int     | Interval between steps, in milliseconds. |
| `stretch`              | Boolean  | Stretch canvas to fit. Comes into play if the `width` and `height` attributes are smaller than the window's dimensions. |
