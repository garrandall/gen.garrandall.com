import * as CONSTANTS from "../shared/constants.js";

export const title = "Larger Than Life";
export const route = "larger-than-life";
export const description = "A generalization of Conway's Game of Life in which cells evolve based on live cells within an <em>r</em>-radius Moore neighbourhood of themselves. The image features the \"Symmetries\" configuration, encoded as <code>R20,B270..368,S452..1021</code>. This simulation includes a handful of pre-configured rule sets heavily inspired by <a href=\"https://www.springer.com/us/book/9783319272696\" target=\"blank\"><em>Designing Beauty: The Art of Cellular Automata</em></a>.";
export const info = "https://en.wikipedia.org/wiki/Life-like_cellular_automaton#Generalizations";
export const source = CONSTANTS.BASE_REPO_URL + "/tree/master/src/" + route;
export const site = CONSTANTS.BASE_SITE_URL;
