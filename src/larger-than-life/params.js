import { Colors } from "three";
import {
  PARAM_TYPES,
  getURLParams
} from "../shared/params.js";

const Init = {
  RANDOM: "random",
  CIRCLE_99: "circle-99"
}
const ColorBehavior = {
  LIVE_ONLY: "live-only",
  FADE_OUT: "fade-out",
  PERSIST: "persist",
  PERSIST_SPECTRUM: "persist-spectrum"
}

const Rules = {
  "conways-game-of-life": {
    label: "Conway's Game of Life",
    rule: [1,3, 3, 3, 4],
    init: Init.RANDOM
  },
  "bugs": {
    label: "Bugs",
    rule: [5, 34, 45, 34, 58],
    init: Init.RANDOM
  },
  "bugs-movie": {
    label: "Bugs Movie",
    rule: [10, 123, 170, 123, 212],
    init: Init.RANDOM
  },
  "suicidal-bugs": {
    label: "Suicidal Bugs",
    rule: [5,33,49,37,59],
    init: Init.RANDOM
  },
  "symmetries": {
    label: "Symmetries",
    rule: [20,270,368,452,1021],
    init: Init.CIRCLE_99
  },
  "waffles": {
    label: "Waffles",
    rule: [7,75,170,100,200],
    init: Init.RANDOM
  },
  "majority": {
    label: "Majority",
    rule: [4,41,81,41,81],
    init: Init.RANDOM
  },
  "x-marks-the-spot": {
    label: "X Marks the Spot",
    rule: [5,1,26,12,98],
    init: Init.CIRCLE_99
  }
}

const paramDefines = {
  width: {
    type: PARAM_TYPES.INT,
    max: window.innerWidth,
    min: window.innerWidth / 32,
    def: window.innerWidth / 2
  },
  height: {
    type: PARAM_TYPES.INT,
    max: window.innerHeight,
    min: window.innerHeight / 32,
    def: window.innerHeight / 2
  },
  stretch: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    label: "Stretch to Fit Screen"
  },
  rule: {
    type: PARAM_TYPES.ENUM,
    def: "bugs",
    options: Object.keys(Rules)
  },
  colorBehavior: {
    type: PARAM_TYPES.ENUM,
    def: ColorBehavior.FADE_OUT,
    options: [...Object.values(ColorBehavior)]
  },
  active: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  },
  interval: {
    type: PARAM_TYPES.INT,
    def: 100,
    hidden: true
  },
  controls: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  }
}

const params = getURLParams(paramDefines);

console.log("PARAMS LOADED:", params);

export {
  params,
  paramDefines,
  Init,
  Rules,
  ColorBehavior
}
