import * as CONSTANTS from "../shared/constants.js";

export const title = "Game of Life";
export const route = "life";
export const description = "The most popular CA game, originally created by John Conway in 1970. The rule pictured is not Conway's original rule (<code>B3/S23</code>), but another called Day & Night, encoded as <code>B3678/S34678</code>.";
export const info = "https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life";
export const source = CONSTANTS.BASE_REPO_URL + "/tree/master/src/" + route;
export const site = CONSTANTS.BASE_SITE_URL;
