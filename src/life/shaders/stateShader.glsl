// N: num states
// X: dimensions of texture in X axis
// Y: dimensions of texture in Y axis
//
// born: num of neighbors to change a cell from dead to alive
// survive: num of neighbors to keep a cell alive
//
// stateTexture: texture2D with state of cells
//
@import ../../shared/shaders/intStateTex;

uniform int born[NUM_B];
uniform int survive[NUM_S];

int getCellState(vec2 cellRef) {
  vec2 safeRef = wrapCellRef(cellRef, resolution.xy);

  vec4 stateTex = texture2D(stateTexture, safeRef / resolution.xy);
  return stateFromTex(stateTex);
}

void main()
{
  int state = getCellState(gl_FragCoord.xy);

  int neighbors = 0;

  for (int dy = -1; dy <= 1; dy++) {
    for (int dx = -1; dx <= 1; dx++) {
      if (dy == 0 && dx == 0) continue;
      if (getCellState(gl_FragCoord.xy + vec2(dx, dy)) > 0) {
        neighbors += 1;
      }
    }
  }

  int newState;

  if (state == 1) {
    for (int i = 0; i < NUM_S; i++) {
      if (survive[i] == neighbors) {
        gl_FragColor = stateToTex(1);
        return;
      }
    }
  } else if (state == 0) {
    for (int i = 0; i < NUM_B; i++) {
      if (born[i] == neighbors) {
        gl_FragColor = stateToTex(1);
        return;
      }
    }
  }
}
