precision highp float;

@import ../../shared/shaders/intStateTex;
@import ../../shared/shaders/hsv2rgb;

uniform sampler2D stateTexture;
uniform vec2 resolution;
uniform vec3 colors[NUM_STATES];

void main() {
  vec4 stateTex = texture2D(stateTexture, gl_FragCoord.xy / resolution.xy);
  int state = stateFromTex(stateTex);

  vec3 color = hsv2rgb(colors[NUM_STATES - 1]);

  for (int i = 0; i < NUM_STATES; i++) {
    if (i == state) color = hsv2rgb(colors[i]);
  }

  gl_FragColor = vec4(color, 1.0);
}
