import * as THREE from "three";

import ColorSchemes from "../shared/color-schemes.js";
import WebGLScene2D from "../shared/webgl-scene-2d.js";
import { params } from "./params";

// shaders
import stateShader from "./shaders/stateShader.glsl";
import vertexShader from "../shared/shaders/passThruVertexShader.glsl";
import fragmentShader from "./shaders/fragmentShader.glsl";
import { parseRule } from "../shared/utils.js";

const STATES = 2;

const colors = ColorSchemes.blackWhite();

const scene = new WebGLScene2D({
  width: params.stretch ? window.innerWidth : params.width,
  height: params.stretch ? window.innerHeight : params.height,
  active: params.active
});

// randomly initialize data
const data = new Float32Array(params.width * params.height * 4);
for (let i = 0; i < params.width * params.height; i++) {
  let r = Math.floor(Math.random() * STATES);
  data[i*4  ] = (r >> 3) & 1;
  data[i*4+1] = (r >> 2) & 1;
  data[i*4+2] = (r >> 1) & 1;
  data[i*4+3] = r & 1;
}

const {born, survive} = parseRule(params.rule);

scene.addComputePass({
  width: params.width,
  height: params.height,
  textureRef: "stateTexture",
  shader: stateShader,
  data,
  uniforms: {
    born         : new THREE.Uniform(new Int32Array(born)),
    survive      : new THREE.Uniform(new Int32Array(survive))
  },
  defines: {
    RANGE: params.range,
    NUM_B: born.length,
    NUM_S: survive.length
  }
});

scene.initComputePasses(params.interval);

scene.initRender({
  fragmentShader,
  vertexShader,
  uniforms: {
    colors: new THREE.Uniform(colors.map(color => ColorSchemes.colorToVec(color)))
  },
  defines: {
    NUM_STATES: STATES
  }
});

export default scene;
