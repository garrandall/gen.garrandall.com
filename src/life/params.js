import {
  PARAM_TYPES,
  getURLParams
} from "../shared/params.js";

const paramDefines = {
  width: {
    type: PARAM_TYPES.INT,
    max: window.innerWidth,
    min: 1,
    def: window.innerWidth / 2
  },
  height: {
    type: PARAM_TYPES.INT,
    max: window.innerHeight,
    min: 1,
    def: window.innerHeight / 2
  },
  rule: {
    type: PARAM_TYPES.STRING,
    def: "B3/S23"
  },
  stretch: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    label: "Stretch to Fit Screen"
  },
  active: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  },
  interval: {
    type: PARAM_TYPES.INT,
    def: 0,
    hidden: true
  },
  controls: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  }
}

const params = getURLParams(paramDefines);

console.log("PARAMS LOADED:", params);

export {
  params,
  paramDefines
}
