# Game of Life

Simulates [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) and other rules in a live WebGL canvas.

![](sample.png)

## URL Parameters

| Parameter       | Type | Description |
|-----------------|------|-------------|
| `width`                | Int      | Width of canvas  |
| `height`               | Int      | Height of canvas |
| `rule`          | String | Rule string as used [here](https://www.conwaylife.com/wiki/Cellular_automaton#Well-known_life-like_cellular_automata) |
| `init`          | `random`\|`single`\|`empty` | Initialization, random by default. |
| `active`        | Boolean | Is simulation running on page load, or paused? Default is true. |
| `interval`      | Int     | Interval between steps, in milliseconds. |
| `stretch`              | Boolean  | Stretch canvas to fit. Comes into play if the `width` and `height` attributes are smaller than the window's dimensions. |
