import {
  PARAM_TYPES,
  getURLParams
} from "../shared/params.js";

const paramDefines = {
  width: {
    type: PARAM_TYPES.INT,
    max: window.innerWidth,
    min: 1,
    def: window.innerWidth / 2
  },
  height: {
    type: PARAM_TYPES.INT,
    max: window.innerHeight,
    min: 1,
    def: window.innerHeight / 2
  },
  n: {
    type: PARAM_TYPES.INT,
    def: 5,
    max: 12,
    min: 1
  },
  threshold: {
    type: PARAM_TYPES.INT,
    def: 2,
    min: 0,
    max: 50
  },
  range: {
    type: PARAM_TYPES.INT,
    def: 1,
    min: 1,
    max: 5
  },
  colors: {
    type: PARAM_TYPES.ENUM,
    def: "kandinsky",
    options: ["kandinsky", "rainbow", "greyscale"]
  },
  stretch: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    label: "Stretch to Fit Screen"
  },
  active: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  },
  interval: {
    type: PARAM_TYPES.INT,
    def: 1,
    hidden: true
  },
  controls: {
    type: PARAM_TYPES.BOOLEAN,
    def: true,
    hidden: true
  }
};

const params = getURLParams(paramDefines);

console.log("PARAMS LOADED:", params);

export {
  params,
  paramDefines
}
