// N: num states
// X: dimensions of texture in X axis
// Y: dimensions of texture in Y axis
//
// threshold: num of neighbors at next states needed to change curr state
// range: radius of neighborhood
//
// stateTexture: texture2D with state of cells
//
@import ../../shared/shaders/intStateTex;

uniform int n;
uniform int neighborhood;
uniform int range;
uniform int threshold;

uniform bool wrapEdges;
uniform int  edgeBehavior;

int getCellState(vec2 cellRef) {
  vec2 safeRef = wrapCellRef(cellRef, resolution.xy);

  vec4 stateTex = texture2D(stateTexture, safeRef / resolution.xy);
  return stateFromTex(stateTex);
}

void main()
{
  int state = getCellState(gl_FragCoord.xy);
  int successor = state + 1;
  if (successor >= n) successor -= n;

  int neighbors = 0;

  for (int dy = -RANGE; dy <= RANGE; dy++) {
    for (int dx = -RANGE; dx <= RANGE; dx++) {
      if (dy == 0 && dx == 0) continue;
      if (getCellState(gl_FragCoord.xy + vec2(dx, dy)) == successor) {
        neighbors += 1;
      }
    }
  }

  int newState = state;

  if (neighbors > threshold) {
    newState += 1;
    if (newState >= n) newState -= n;
  }

  gl_FragColor = stateToTex(newState);
}
