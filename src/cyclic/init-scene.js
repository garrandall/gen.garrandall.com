import * as THREE from "three";

import ColorSchemes from "../shared/color-schemes.js";
import WebGLScene2D from "../shared/webgl-scene-2d.js";
import { params } from "./params.js";

// shaders
import stateShader from "./shaders/stateShader.glsl";
import vertexShader from "../shared/shaders/passThruVertexShader.glsl";
import fragmentShader from "./shaders/fragmentShader.glsl";

const scene = new WebGLScene2D({
  width: params.stretch ? window.innerWidth : params.width,
  height: params.stretch ? window.innerHeight : params.height,
  active: params.active
});

let colors;

if (params.colors === "kandinsky") colors = ColorSchemes.kandinsky({ n: params.n });
if (params.colors === "rainbow")   colors = ColorSchemes.getEquidistantColorSet({ n: params.n });
if (params.colors === "greyscale") colors = ColorSchemes.shades(params.n);

// randomly initialize data
const data = new Float32Array(params.width * params.height * 4);
for (let i = 0; i < params.width * params.height; i++) {
  let r = Math.floor(Math.random() * params.n);
  data[i*4  ] = (r >> 3) & 1;
  data[i*4+1] = (r >> 2) & 1;
  data[i*4+2] = (r >> 1) & 1;
  data[i*4+3] = r & 1;
}

scene.addComputePass({
  width: params.width,
  height: params.height,
  textureRef: "stateTexture",
  shader: stateShader,
  data,
  uniforms: {
    n            : new THREE.Uniform(params.n),
    threshold    : new THREE.Uniform(params.threshold),
    range        : new THREE.Uniform(params.range)
  },
  defines: {
    RANGE: params.range
  }
});

scene.initComputePasses(params.interval);

scene.initRender({
  fragmentShader,
  vertexShader,
  uniforms: {
    colors: new THREE.Uniform(colors.map(color => ColorSchemes.colorToVec(color)))
  },
  defines: {
    NUM_STATES: params.n
  }
});

export default scene;
