import * as CONSTANTS from "../shared/constants.js";

export const title = "Cyclic CA";
export const route = "cyclic";
export const description = "A CA game in which a cell takes on the next state if it has enough neighbors with the next state. These often converge to spiraling patterns.";
export const info = "https://en.wikipedia.org/wiki/Cyclic_cellular_automaton";
export const source = CONSTANTS.BASE_REPO_URL + "/tree/master/src/" + route;
export const site = CONSTANTS.BASE_SITE_URL;
