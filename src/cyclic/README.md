# Cyclic Cellular Automaton

Simulates [cyclic cellular automata](https://en.wikipedia.org/wiki/Cyclic_cellular_automaton).

Ciyclic Cellular Automata were heavily researched by David Griffeath. His sample pieces included in [*Designing Beauty: The Art of Cellular Automata*](https://www.springer.com/us/book/9783319272696) inspired me to implement it myself.

This simulation uses WebGL via ThreeJS.

![](sample.png)

## URL Parameters

| Parameter              | Type     | Description      |
|------------------------|----------|------------------|
| `width`                | Int      | Width of canvas  |
| `height`               | Int      | Height of canvas |
| `n`                    | Int      | Number of states |
| `threshold`            | Int      | If cell has (> threshold) neighbors with next state, the cell with advance to the next state |
| `range`                | Int      | Radius of neighborhood. Recommended to be 1 or 2 |
| `active`               | Boolean  | is simulation running on load? |
| `interval`             | Int      | time interval to enforce between updates in milliseconds |
| `stretch`              | Boolean  | Stretch canvas to fit. Comes into play if the `width` and `height` attributes are smaller than the window's dimensions.
