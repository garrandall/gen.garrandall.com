const path = require('path');

const mode = process.env.NODE_ENV || 'development';

module.exports = {
  resolve: {
    alias: {
      svelte: path.resolve("node_modules", "svelte")
    },
    extensions: ['.mjs', '.js', '.svelte'],
    mainFields: ['svelte', 'browser', 'module', 'main']
  },
  entry: {
    "elementary": "./src/elementary/index.js",
    "life": "./src/life/index.js",
    "larger-than-life": "./src/larger-than-life/index.js",
    // "flocking": "./src/flocking/index.js", // incomplete, weird behavior
    "cyclic": "./src/cyclic/index.js",
    "flow-field": "./src/flow-field/index.js",
    ".": "./src/index.js"
  },
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "[name]/bundle.js"
  },
  devServer: {
    contentBase: path.resolve(__dirname, "build")
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/env"]
          }
        }
      },
      {
        test: /\.(html|css|png)$/i,
        use: {
          loader: 'file-loader',
          options: {
            outputPath: (url, resourcePath, context) => {
              let dirs = resourcePath.split("/src/")[1];
              dirs = dirs.split("/");
              const dir = dirs.length > 1 ? dirs[dirs.length - 2] : "";
              return `${dir}/${url}`;
            },
            name: "[name].[ext]",
            hotReload: true
          }
        },
      },
      {
        test: /\.svelte$/i,
        use: {
          loader: 'svelte-loader',
          options: {
            hotReload: false
          }
        }
      },
      {
        test: /\.glsl$/,
        use: [
          {
            loader: 'webpack-glsl-loader'
          }
        ]
      }
    ]
  },
  mode
};

if (mode === "production") {
  module.exports.plugins = [
    new webpack.optimize.CommonsChunkPlugin('common.js'),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.AggressiveMergingPlugin()
  ];
}
