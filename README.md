# Generative Art and Automata

This repository build a collection of static web pages which simulate various generative and artificial life concepts. Currently, the following pages are implemented:

- `/elementary`: Simulates a 1-dimensional [Elementary Cellular Automaton](https://en.wikipedia.org/wiki/Elementary_cellular_automaton)
- `/life`: Simulates [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
- `/larger-than-life`: Simulates Larger-Than-Life cellular automata.
- `/cyclic`: Simulates [Cyclic Cellular Automata](https://en.wikipedia.org/wiki/Cyclic_cellular_automaton)
- `/flow-field`: Simulates particles on [Vector Fields](https://en.wikipedia.org/wiki/Vector_field)

Simulations use either [p5.js](https://p5js.org), a drawing library inspired by [Processing](https://processing.org), or WebGL via [ThreeJS](https://threejs.org).

## Installation

Pull the repository and install its packages:

```
npm install
```

## Development

`webpack-dev-server` is used to hot-reload during development.

```
npm run dev
open http://localhost:8080
```

## Deployment

```
npm run build
```

Each page is transpiled seperately, as some use ThreeJS and some use P5js. Deploying the `build/` directory is sufficient.

## Future Ideas

- Langton's Ant
- Image Processing with CAs
- [Dithering](https://www.youtube.com/watch?v=IviNO7iICTM)
- Fluid Simulation
- L-Systems
- Voronoi Diagram

For inspiration, see [generated.space](https://generated.space/), [On Generative Algorithms](https://inconvergent.net/generative), and examples from [*Designing Beauty: The Art of Cellular Automata*](https://www.springer.com/us/book/9783319272696).
